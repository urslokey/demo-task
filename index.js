const express = require('express');
const app = express();
const db = require('./config/database');
const cors = require('cors');
const register = require('./controller/register');
const login = require('./controller/login');
const verifyToken = require('./middleware/verifyToken');
const { fetchProducts, createProduct ,updateProduct , deleteProduct } = require('./controller/product');


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));


/** End points */
app.post('/register',register);
app.post('/login',login);

/** CURD operation end points */
app.post('/product',verifyToken,createProduct);
app.get('/product',verifyToken,fetchProducts);
app.put('/product',verifyToken,updateProduct);
app.delete('/product/:id',verifyToken,deleteProduct);



app.listen("3000",()=>{
    console.log("App running at 3000 port")
})