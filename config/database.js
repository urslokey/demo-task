const knex = require('knex')({
    client: 'mysql',
    connection: {
      host : 'localhost',
      user : 'root',
      password : '',
      database : 'crm',
    },
    pool: { min: 0, max: 7 },
    acquireConnectionTimeout: 10000,
  });


  module.exports = knex;
