const db = require('./database');
const selectRawQuery = async(query)=>{
    return new Promise(async(resolve,reject)=>{
        try{
            db.raw(query).then((result) => {
                resolve(result[0]);
            }).catch((error) => {
                reject(error);
            });
        }catch(err){
            reject(err);
        }
    })
}

const create = async(table , data)=>{
    return new Promise(async(resolve,reject)=>{
        try{
            db(table).insert(data).then((result) => {
                resolve(result);
            }).catch((error) => {
                reject(error);
            });
        }catch(err){
            reject(err);
        }
    })
}

const update = async(table,where,data)=>{
    return new Promise(async(resolve,reject)=>{
        try{
            db(table).where(where).update(data).then((result) => {
                resolve(result);
            }).catch((error) => {
                reject(error);
            });
        }catch(err){
            reject(err);
        }
    })
}

const deleteRecord = async(table,where)=>{
    return new Promise(async(resolve,reject)=>{
        try{
            db(table).where(where).del().then((result) => {
                resolve(result);
            }).catch((error) => {
                reject(error);
            });
        }catch(err){
            reject(err);
        }
    })
}
module.exports = { selectRawQuery,create,update,deleteRecord };