var jwt = require('jsonwebtoken');

const verifyToken = (req,res,next) =>{
    try {
        let token = req.headers.authorization.split('Bearer ')[1];
        var decoded = jwt.verify(token, 'token');
        req.role = decoded.role;
        next();
      } catch(err) {
        res.statusCode = 400;
        res.json({ message : "Invalid token." });
      }
}

module.exports = verifyToken;