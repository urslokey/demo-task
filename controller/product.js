const { selectRawQuery,create,update,deleteRecord } = require('../config/queries');

const fetchProducts = async(req,res) =>{
    try{
        if(req.role === 'admin' || req.role === 'manager') { /** Validating role */
            const products = await selectRawQuery(`select * from products`);
            res.statusCode = 200;
            res.json({ message : "Success.",data:products });
            return;
        }else{
            res.statusCode = 403;
            res.json({ message : "You don't have access to this resource." });
            return;
        }

    }catch(error){
        res.statusCode = 400;
        res.json({ message : "Unable perform your request." });
    }
}

const createProduct = async(req,res) =>{
    try{
        const title = req.body.title;
        const description = req.body.description;
        const inventoryCount = req.body.inventory_count;

         /** Validating all params */
        if(!title || !description || !inventoryCount){
            res.statusCode = 400;
            res.json({ message : "Required all params." });
            return;
        }

        if(req.role === 'admin' ) { /** Validating role */
            await create('products',{ title,description, inventory_count:inventoryCount });
            res.statusCode = 201;
            res.json({ message : "Success." });
            return;
        }else{
            res.statusCode = 403;
            res.json({ message : "You don't have access to this resource." });
            return;
        }
    }catch(error){
        res.statusCode = 400;
        res.json({ message : "Unable perform your request." });
    }
}
const updateProduct = async(req,res) =>{
    try{
        const id = req.body.id;
        const title = req.body.title;
        const description = req.body.description;
        const inventoryCount = req.body.inventory_count;
    
         /** Validating param */
         if(!id){
            res.statusCode = 400;
            res.json({ message : "Required id param." });
            return;
        }

        if(req.role === 'admin' || req.role === 'manager') { /** Validating role */
            const updateProduct = {};
            if(title){
                updateProduct.title = title;
            }
            if(description){
                updateProduct.description = description;
            }
            if(inventoryCount || inventoryCount == 0 ){
                updateProduct.inventory_count = inventoryCount;
            }
            await update('products',{ id },updateProduct);
            res.json({ message : "Success." });
            return;
        }else{
            res.statusCode = 403;
            res.json({ message : "You don't have access to this resource." });
            return;
        }
    }catch(error){
        res.statusCode = 400;
        res.json({ message : "Unable perform your request." });
    }
}

const deleteProduct = async(req,res) =>{
    try{
        const id = req.params.id;

        /** Validating param */
         if(!id){
            res.statusCode = 400;
            res.json({ message : "Required id param." });
            return;
        }

        if(req.role === 'admin') { /** Validating role */
            await deleteRecord('products',{ id })
            res.json({ message : "Success." });
            return;
        }else{
            res.statusCode = 403;
            res.json({ message : "You don't have access to this resource." });
            return;
        }
    }catch(error){
        res.statusCode = 400;
        res.json({ message : "Unable perform your request." });
    }
}


module.exports = { fetchProducts, createProduct ,updateProduct , deleteProduct };