const bcrypt = require('bcrypt');
const { selectRawQuery,create } = require('../config/queries');
var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;


const register = async (req, res) => {
    try{
        const username = req.body.username;
        const email = req.body.email;
        let password = req.body.password;
        const role = req.body.role;

        /** Validating all params */
        if(!username || !email || !password || !role){
            res.statusCode = 400;
            res.json({ message : "Required all params." });
            return;
        }
     
         /** Validating email */
        if(!validRegex.test(email)){
            res.statusCode = 400;
            res.json({ message : "Required valid email address." });
            return;
        }

        /** Validating role */
        if(!(role == 'admin' || role == 'manager' || role == 'staff')){
            res.statusCode = 400;
            res.json({ message : "Required valid user role." });
            return;
        }

        /** Validating username or email in exist or not */
        const checkUser = await selectRawQuery(`select * from users where username='${username}' or email='${email}'`);
        if(checkUser.length){
            res.statusCode = 409;
            res.json({ message : "Username or Email already exist" });
            return;
        }

        /** Password hashing */
        password = bcrypt.hashSync(req.body.password, 10);

        /** Create user */
        await create('users',{ username,email,password,role});

        res.statusCode = 201;
        res.json({ message : "User created successfully." });
    }catch(error){
        res.statusCode = 400;
        res.json({ message : "Unable perform your request." });
    }
}


module.exports = register;