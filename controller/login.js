const bcrypt = require('bcrypt');
const { selectRawQuery } = require('../config/queries');
var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
var jwt = require('jsonwebtoken');

const login =async (req,res) =>{
    try{
        const email = req.body.email;
        const password = req.body.password;

        /** Validating all params */
        if(!email || !password){
            res.statusCode = 400;
            res.json({ message : "Required all params." });
            return;
        }
    
         /** Validating email */
        if(!validRegex.test(email)){
            res.statusCode = 400;
            res.json({ message : "Required valid email address." });
            return;
        }


        /** Validating username or email is exist or not */
        const checkUser = await selectRawQuery(`select * from users where email='${email}'`);
        if(checkUser.length == 0){
            res.statusCode = 401;
            res.json({ message : "Invalid email." });
            return;
        }

        /** Validating hash password with original */
        const validatePassword = bcrypt.compareSync(password, checkUser[0].password);
        if(!validatePassword){
            res.statusCode = 401;
            res.json({ message : "Incorrect password." });
            return;
        }

        /** Generate token */
        var token = jwt.sign({ id:checkUser[0].id ,role:checkUser[0].role }, 'token');
 
        res.statusCode = 200;
        res.json({ message : "Login successfully",token : token });
    }catch(error){
        res.statusCode = 400;
        res.json({ message : "Unable perform your request." });
    }
}


module.exports = login;